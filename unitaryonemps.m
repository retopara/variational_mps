function [mps, NU] = unitaryonemps(mps, direction)
% ---------------------------------------------------------------
%  [U, NU, DS] = unitary_onesite(mps, direction)
%
%  Perform a singular value decomposition on one mps
%
%  Parameters
%    mps = The mps to be operated
%    direction = 'lr' left to right, or 'rl' right to left
%    isite = The site of mps
%
%  Returns
%    U = Unitary mps
%    NU = Non-unitary mps
%    DS = the number of singular values
%
%  Attention! Must use Economic SVD in this function, so that D will not
%  grow exponentially!
% --------------------------------------------------------------

switch direction
    % svd from left to right, keep the left unitary
    case 'lr'
        Astr = split('L,P');
        Lidx = find1(mps, Astr);
        Ridx = 1:length(mps.index);
        Ridx(Lidx) = [];
        Asize = [mps.size(Lidx), mps.size(Ridx)];
        Lsize = prod(mps.size(Lidx));
        Rsize = prod(mps.size(Ridx));
        mps = permutemps(mps, [Lidx, Ridx]);
        t = reshape(mps.tensor, [Lsize, Rsize]);
        [uleft, s, uright] = svd(t, 'econ');
        uright = uright';
        NU.tensor = s * uright;
        NU.size = size(NU.tensor);
        NU.name = split('NU,NU');
        NU.index = split('L,R');
        mps.tensor = reshape(uleft, Asize);
        mps.size = size(mps.tensor);
        
    % svd from right to left, keep the right unitary    
    case 'rl'
        Astr = split('R,P');
        Ridx = find1(mps, Astr);
        Lidx = 1:length(mps.index);
        Lidx(Ridx) = [];
        Asize = [mps.size(Lidx), mps.size(Ridx)];
        Lsize = prod(mps.size(Lidx));
        Rsize = prod(mps.size(Ridx));
        mps = permutemps(mps, [Lidx, Ridx]);
        t = reshape(mps.tensor, [Lsize, Rsize]);
        [uleft, s, uright] = svd(t, 'econ');
        uright = uright';
        NU.tensor = uleft * s;
        NU.size = size(NU.tensor);
        NU.name = split('NU,NU');
        NU.index = split('L,R');
        mps.tensor = reshape(uright, Asize);
        mps.size = size(mps.tensor);
        
    otherwise
        error('unitaryonemps:WrongDirection', '\n [unitaryonemps] ERROR: Wrong direction %s!', direction);
end