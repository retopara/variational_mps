function X = contracttensors(A, Astr, B, Bstr)

Astr = split(Astr);
Bstr = split(Bstr);
Alen = length(A.name);
Blen = length(B.name);

Aridx = find2(A, Astr);
Blidx = find2(B, Bstr);

Alidx = 1:Alen;
Alidx(Aridx) = [];
Alsize = A.size(Alidx);
Arsize = A.size(Aridx);

Bridx = 1:Blen;
Bridx(Blidx) = [];
Blsize = B.size(Blidx);
Brsize = B.size(Bridx);

testAr = prod(Arsize);
testBl = prod(Blsize);
if testAr ~= testBl
    error('contracttensors:RightLeftDimNotMatch', '\n [contracttensors] ERROR: A_right_size %d ~= B_left_size %d', testAr, testBl);
end

Atmp = permute(A.tensor, [Alidx, Aridx]);
Atmp = reshape(Atmp, [prod(Alsize), prod(Arsize)]);
Btmp = permute(B.tensor, [Blidx, Bridx]);
Btmp = reshape(Btmp, [prod(Blsize), prod(Brsize)]);
X.size = [Alsize, Brsize];
X.tensor = reshape(Atmp*Btmp, X.size);
X.name = [A.name(Alidx), B.name(Bridx)];
X.index = [A.index(Alidx), B.index(Bridx)];