function mat = tensor2mat(t, Lstr, Rstr)

Lstr = split(Lstr);
Rstr = split(Rstr);
Lidx = find2(t, Lstr);
Ridx = find2(t, Rstr);
Lsize = prod(t.size(Lidx));
Rsize = prod(t.size(Ridx));
mat = permute(t.tensor, [Lidx, Ridx]);
mat = reshape(mat, [Lsize, Rsize]);
% [Lidx, Ridx]
% [Lsize, Rsize]