function [D] = checkD(N, d, Dmax)
% ---------------------------------------------------------------
%  [schcut] = checkD(N, d, D)
%
%  Return the max possible schcut based on N, d, and Dmax
%
%  Parameters
%    N          = The number of particles in 1D system
%    d          = The dim of physical indices
%    Dmax       = The max wanted dim of virtual indices
%
%  Returns
%    schcut(N-1)= The array of max possible schcut
% ---------------------------------------------------------------

% verify the schcut
D = zeros(N+1, 1);
for i = 0:N
    D(i+1) = min(d^(i), d^(N-i));
    D(i+1) = min(D(i+1), Dmax);
end
D(N+1) = 1;