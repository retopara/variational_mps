function mps = conjt(mps)

mps.tensor = conj(mps.tensor);
for i = 1:length(mps.name)
    mps.name{i} = [mps.name{i}, '*'];
end