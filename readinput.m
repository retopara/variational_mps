function [N, d, D, cplx, prtToFile, maxsweep, Htype, J, h, tol, str] = readinput()
% ---------------------------------------------------------------
%  [N, d, D, eigencut,...
%     tstep1, Treal, Nstep1, Nstep2, Nrange, torder, cplx,...
%     Htype, h0, hT, hsmall, alpha, prtToFile,...
%     str] = readinput()
% 
%  Read input for MASTER EQUATION TEBD program, store in the struct: input
% 
%  Returns
%    The input values, and the display string for input values
% ---------------------------------------------------------------
fid = fopen('input', 'r');

str = '';
name = fscanf(fid, '%s', 1);
while (isempty(name) == 0) && (strcmp(name, '%%%%%') == 0) && (strcmp(name, '%') == 0)
    equalmark = fscanf(fid, '%s', 1);
    switch name
        % below are in format %d
        case 'N'
            tmp = fscanf(fid, '%d');
            N = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
        case 'd'
            tmp = fscanf(fid, '%d');
            d = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
        case 'D'
            tmp = fscanf(fid, '%d');
            D = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
        case 'maxsweep'
            tmp = fscanf(fid, '%d');
            maxsweep = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
        case 'prtToFile'
            tmp = fscanf(fid, '%d');
            prtToFile = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
        case 'cplx'
            tmp = fscanf(fid, '%d');
            cplx = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
            
            % below are in format %f
        case 'J'
            tmp = fscanf(fid, '%f');
            J = tmp;
            str = [str, sprintf('\n %s = %f', name, tmp)];
        case 'h'
            tmp = fscanf(fid, '%f');
            h = tmp;
            str = [str, sprintf('\n %s = %f', name, tmp)];
           
            % below are in format %e
        case 'tol'
            tmp = fscanf(fid, '%e');
            tol = tmp;
            str = [str, sprintf('\n %s = %d', name, tmp)];
            
            % below are in format %s
        case 'Htype'
            tmp = fscanf(fid, '%s', 1);
            Htype = tmp;
            str = [str, sprintf('\n %s = %s', name, tmp)];
            
        otherwise
            error('readinput:WrongInputNameFormat', '\n [readinput] ERROR: Wrong input name %s!', name);
    end
    name = fscanf(fid, '%s', 1);
end