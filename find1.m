function idx = find1(A, str)

idx = [];
for i = 1:length(str)
    found = 0;
    for j = 1:length(A.name)
        if strcmp(str{i}, A.index{j})
            found = 1;
            idx = [idx, j];
            break;
        end
    end
    if ~found
        error('find1:StrNotFound', '\n [find1] ERROR: Cannot find index %s_%s\n', str{i}, str{i+1});
    end
end