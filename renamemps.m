function mps = renamemps(mps, oldstr, newstr)

oldstr = split(oldstr);
newstr = split(newstr);
idx = find2(mps, oldstr);
for i = 1:2:length(newstr)
    mps.name{idx} = newstr{i};
    mps.index{idx} = newstr{i+1};
end