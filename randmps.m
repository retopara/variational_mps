function [mps] = randmps(d, N, D, cplx)
% ------------------------------------------------------
%  [mps] = randmps(d, N, schcut, cplx)
%
%  Create random mps
%
%  Parameters
%   N           = Length of the MPS string
%   schcut      = The dimension of virtual indices
%   d           = The dimension of physical indices
%
%  Returns
%   mps         = The mps with open boundary
% -------------------------------------------------------
mps = cell(N, 1);
for i = 1:N
    mps{i}.size = [D(i+1), d, D(i)];
    mps{i}.tensor = rand(mps{i}.size);
    if cplx
        mps{i}.tensor = mps{i}.tensor + 1i*rand(mps{i}.size);
    end
    mps{i}.name = split(istr('Ai,Ai,Ai', i));
    mps{i}.index = split('R,P,L');
end