function idx = find2(A, str)

idx = [];
for i = 1:2:length(str)
    found = 0;
    for j = 1:length(A.name)
        if strcmp(str{i}, A.name{j}) && strcmp(str{i+1}, A.index{j})
            found = 1;
            idx = [idx, j];
            break;
        end
    end
    if ~found
        error('find2:StrNotFound', '\n [find2] ERROR: Cannot find index %s_%s\n', str{i}, str{i+1});
    end
end