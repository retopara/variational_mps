function idx = findhset(label, hset)

% idx = 0;
for i = 1:length(hset)
    if isempty(hset{i})
        error('findhset:LabelNotFound', '\n [findhset] ERROR: label %s not found!\n', label);
    elseif strcmp(label, hset{i}.label)
        idx = i;
        break;
    end
end
% if idx == 0
%     fprintf('\n [findhset] ERROR: label %s not found!', label);
% end