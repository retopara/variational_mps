% function [hright] = initHstorage(mps, hset, d, N, M)
% ---------------------------------------------------------------
%  [hright] = initHstorage(mps, hset, usepbc, D)
%
%  Initialize hright necessary to calculate Heff in the sweep
%  variational scheme
%
%  Parameters
%    mps = The mps state
%    hset = The hamiltonian
%    usepbc = 1- use periodic boundary condition 0- open boundary
%    D = The dimension of virtual bond
%
%  Returns
%    hright = The NxM tensors stroages used in x sweeping
% --------------------------------------------------------------
Rid.tensor = 1;
Rid.size = [1, 1];
Rid.name = split(istr('Ai+1,Ai+1*', N));
Rid.index = split('L,L');

Lid.tensor = 1;
Lid.size = [1, 1];
Lid.name = split(istr('Ai-1,Ai-1*', 1));
Lid.index = split('R,R');

hright = cell(M, N);
hleft = cell(M, N);
for m = 1:M
    hright{m, N} = Rid;
    hleft{m, 1} = Lid;
end

% initialize hright for the first time run
for m = 1:M
    i = N;
    t = mps{N};
    if ~isempty(hlabel{m, i})
        Astr = istr('Ai,P', i);
        Bstr = istr('H,U', i);
        idx = findhset(hlabel{m, i}, hset);
        t = contracttensors(mps{N}, Astr, hset{idx}, Bstr);
        Astr = istr('Ai,R,H,D', i);
    else
        Astr = istr('Ai,R,Ai,P', i);
    end
    Bstr = istr('Ai*,R,Ai*,P', i);
    hright{m, i-1} = contracttensors(t, Astr, conjt(mps{N}), Bstr);
        
    for i = N:-1:2
        Astr = istr('Ai,R', i);
        Bstr = istr('Ai+1,L', i);
        t = contracttensors(mps{i}, Astr, hright{m, i}, Bstr);
        if ~isempty(hlabel{m, i})
            Astr = istr('Ai,P', i);
            Bstr = istr('H,U', i);
            idx = findhset(hlabel{m, i}, hset);
            t = contracttensors(t, Astr, hset{idx}, Bstr);
            t = renamemps(t, 'H,D', istr('Ai,P', i));
        end
        Astr = istr('Ai+1*,L,Ai,P', i);
        Bstr = istr('Ai*,R,Ai*,P', i);
        hright{m, i-1} = contracttensors(t, Astr, conjt(mps{i}), Bstr);
    end
end   