% unitarymps
% ---------------------------------------------------------------
%  [mps] = unitary_mps(mps, direction, N)
%
%  Prepares a MPS using gauge transformation such that Neff is the
%  identity, i.e. the MPS is left/right unitary for the last/first site.
%
%  Parameters
%    mps = The mps to be operated
%    direction = 'lr' left to right, or 'rl' right to left
%
%  Returns
%    mps = The mps after gauge transformation
%
% --------------------------------------------------------------

switch direction
    case 'lr'
        for i = 1:N-1
            [mps{i}, NU] = unitaryonemps(mps{i}, 'lr');
            Astr = istr('Ai+1,L', i);
            mps{i+1} = contracttensors(NU, 'NU,R', mps{i+1}, Astr);
            str = istr('Ai+1,L', i);
            mps{i+1} = renamemps(mps{i+1}, 'NU,L', str);
        end
        % last one, how?
    case 'rl'
        for i = N:-1:2
            [mps{i}, NU] = unitaryonemps(mps{i}, 'rl');
            Astr = istr('Ai-1,R', i);
            mps{i-1} = contracttensors(mps{i-1}, Astr, NU, 'NU,L');
            str = istr('Ai-1,R', i);
            mps{i-1} = renamemps(mps{i-1}, 'NU,R', str);
        end
end