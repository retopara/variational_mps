function str = istr(str, i)

% the replace order is very important
% must go from the largest to the smallest
str = strrep(str, 'i+1', num2str(i+1));
str = strrep(str, 'i-1', num2str(i-1));
str = strrep(str, 'i', num2str(i));
