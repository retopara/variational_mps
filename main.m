function main()

clc;
clear;

tictot = tic;

[N, d, D, cplx, prtToFile, maxsweep, Htype, J, h, tol, str] ...
    = readinput();

if prtToFile == 1
    fprintf('\n running...');
    fnameScr = 'screen_out';
    fidScr = fopen(fnameScr, 'w');
    fprintf(fidScr, '\n Screen prints will be output to file: %s', fnameScr);
else
    fidScr = 1;
    fprintf(fidScr, '\n Screen prints will NOT be output to file');
end

fprintf(fidScr, '\n Variational MPS 1D Program Starting :)');
fprintf(fidScr, '\n Start Date&Time is %s', datestr(now));
fprintf(fidScr, '\n The input parameters are:%s', str);
fprintf(fidScr, '\n');

sitelist = [1:N-1, N:-1:2];
Esave = zeros(length(sitelist), 1);

D = checkD(N, d, D);
fprintf(fidScr, '\n Max possible D are:\n ');
fprintf(fidScr, '%d,', D);
fprintf(fidScr, '\n');
mps = randmps(d, N, D, cplx);
direction = 'rl';
unitarymps;

[hset, hlabel, M] = getOBSet(N, Htype, J, h);
initHstorage;

fprintf(fidScr, '\n Sweeping ...');
for isweep = 1:maxsweep
    % from left to right
    for isite = 1:length(sitelist)
        i = sitelist(isite);
        % calculate Heff
        Heff = 0;
        for m = 1:M
            t = kronmps(hleft{m, i}, hright{m, i});
            if ~isempty(hlabel{m, i})
                idx = findhset(hlabel{m, i}, hset);
                t = kronmps(t, hset{idx});
            else
                t = kronmps(t, geteye(d));
            end
            Astr = istr('Ai+1*,L,H,D,Ai-1*,R', i);
            Bstr = istr('Ai+1,L,H,U,Ai-1,R', i);
            Heff = Heff + tensor2mat(t, Astr, Bstr);
        end
        
        % solve eigs
        Heff = (Heff + Heff')/2.0;
        [eigV, eigE] = eigs(Heff, 2);
        [eigE, index] = sort(diag(eigE), 'ascend');
        E = eigE(1, 1);
        fprintf(fidScr, '\n i = %d\tE = %f', i, E);
        Esave(isite) = E;
        X = eigV(:, index(1));
        X = X/sqrt(X' * X);
        mps{i}.size = [D(i+1), d, D(i)];
        mps{i}.tensor = reshape(X, mps{i}.size);
        mps{i}.index = split('R,P,L');
        mps{i}.name = split(istr('Ai,Ai,Ai', i));
        
        % unitary
        if isite <= N-1
            direction = 'lr';
            next = i+1;
            Astr = istr('Ai+1,L', i);
            NUstr1 = 'NU,L';
            NUstr2 = 'NU,R';
        else
            direction = 'rl';
            next = i-1;
            Astr = istr('Ai-1,R', i);
            NUstr2 = 'NU,L';
            NUstr1 = 'NU,R';
        end
        [mps{i}, NU] = unitaryonemps(mps{i}, direction);
        mps{next} = contracttensors(NU, NUstr2, mps{next}, Astr);
        mps{next} = renamemps(mps{next}, NUstr1, Astr);
        
        if isite <= N-1
            % update Hleft and clear Hright
            hleft = updateHleft(i, mps{i}, hleft, hset, hlabel, M);
            for m = 1:M
                hright{m, i} = [];
            end
        else
            % update Hright and clear Hleft
            hright = updateHright(i, mps{i}, hright, hset, hlabel, M);
            for m = 1:M
                hleft{m, i} = [];
            end
        end
    end
    fprintf(fidScr, '\n');
    
    stdi = std(Esave);
    meani = mean(Esave);
    conv = stdi/abs(meani);
    if conv < tol
        fprintf(fidScr, '\n Converge!');
        fprintf(fidScr, '\n stdi = %e, meani = %f, conv = %e', stdi, meani, conv);
        break;
    end
end

% normalize in advance
OBtype = 'ones';
[obset, oblabel, M] = getOBSet(N, OBtype, J, h);
norm = calOB(mps, N, obset, oblabel, M);
fprintf(fidScr, '\n Normalize constant = %f', norm);
mps{1}.tensor = mps{1}.tensor/sqrt(norm);
% calculate observables
fprintf(fidScr, '\n Observables:');
OBtype = Htype;
[obset, oblabel, M] = getOBSet(N, OBtype, J, h);
ob = calOB(mps, N, obset, oblabel, M)/N;
fprintf(fidScr, '\n persite %s = %f', OBtype, ob);
OBtype = 'sxx';
[obset, oblabel, M] = getOBSet(N, OBtype, J, h);
ob = calOB(mps, N, obset, oblabel, M)/N;
fprintf(fidScr, '\n persite %s = %f', OBtype, ob);
OBtype = 'szz';
[obset, oblabel, M] = getOBSet(N, OBtype, J, h);
ob = calOB(mps, N, obset, oblabel, M)/N;
fprintf(fidScr, '\n persite %s = %f', OBtype, ob);
% calculate state vector
vec = calPsiVec(mps, N);
fprintf(fidScr, '\n Size of |Psi> vector is %d x %d', size(vec, 1), size(vec, 2));

fnameSave = 'result.mat';
save(fnameSave);
tot = toc(tictot);
fprintf(fidScr, '\n Variational MPS 1D Program Finish!');
fprintf(fidScr, '\n Results saved in %s', fnameSave);
fprintf(fidScr, '\n Finish Date&Time is %s', datestr(now));
fprintf(fidScr, '\n Total running time %f seconds\n', tot);

if prtToFile == 1
    fclose(fidScr);
    fprintf('\n finish!');
end
