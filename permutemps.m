function mps = permutemps(mps, order)

mps.size = mps.size(order);
mps.tensor = permute(mps.tensor, order);
mps.name = mps.name(order);
mps.index = mps.index(order);