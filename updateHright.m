function hright = updateHright(isite, mps, hright, hset, hlabel, M)

for m = 1:M
    Astr = istr('Ai,R', isite);
    Bstr = istr('Ai+1,L', isite);
    t = contracttensors(mps, Astr, hright{m, isite}, Bstr);
    if ~isempty(hlabel{m, isite})
        Astr = istr('Ai,P', isite);
        Bstr = istr('H,U', isite);
        idx = findhset(hlabel{m, isite}, hset);
        t = contracttensors(t, Astr, hset{idx}, Bstr);
        t = renamemps(t, 'H,D', istr('Ai,P', isite));
    end
    Astr = istr('Ai+1*,L,Ai,P', isite);
    Bstr = istr('Ai*,R,Ai*,P', isite);
    hright{m, isite-1} = contracttensors(t, Astr, conjt(mps), Bstr);
end