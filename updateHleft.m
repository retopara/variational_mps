function hleft = updateHleft(isite, mps, hleft, hset, hlabel, M)

for m = 1:M
    Astr = istr('Ai-1,R', isite);
    Bstr = istr('Ai,L', isite);
    t = contracttensors(hleft{m, isite}, Astr, mps, Bstr);
    if ~isempty(hlabel{m, isite})
        Astr = istr('Ai,P', isite);
        Bstr = istr('H,U', isite);
        idx = findhset(hlabel{m, isite}, hset);
        t = contracttensors(t, Astr, hset{idx}, Bstr);
        t = renamemps(t, 'H,D', istr('Ai,P', isite));
    end
    Astr = istr('Ai-1*,R,Ai,P', isite);
    Bstr = istr('Ai*,L,Ai*,P', isite);
    hleft{m, isite+1} = contracttensors(t, Astr, conjt(mps), Bstr);
end

% if isite == 1
%     for m = 1:M
%         t = mps;
%         if ~isempty(hlabel{m, isite})
%             Astr = istr('Ai,P', isite);
%             Bstr = istr('H,U', isite);
%             idx = findhset(hlabel{m, isite}, hset);
%             t = contracttensors(t, Astr, hset{idx}, Bstr);
%             Astr = istr('Ai,L,H,D', isite);
%         else
%             Astr = istr('Ai,L,Ai,P', isite);
%         end
%         Bstr = istr('Ai*,L,Ai*,P', isite);
%         hleft{m, isite+1} = contracttensors(t, Astr, conjt(mps), Bstr);
%     end
% else

% end