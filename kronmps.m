function mps = kronmps(A, B)

tA = reshape(A.tensor, [prod(A.size), 1]);
tB = reshape(B.tensor, [prod(B.size), 1]);
% tA = kron(tA, tB);
% Matlab built-in function kron must be reverted, this is tricky
tA = kron(tB, tA);
mps.size = [A.size, B.size];
mps.tensor = reshape(tA, mps.size);
mps.name = [A.name, B.name];
mps.index = [A.index, B.index];