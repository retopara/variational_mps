function ob = calOB(mps, N, obset, oblabel, M)

Lid.tensor = 1;
Lid.size = [1, 1];
Lid.name = split(istr('Ai-1,Ai-1*', 1));
Lid.index = split('R,R');

ob = 0;
for m = 1:M
    t = Lid;
    for i = 1:N
        t = contracttensors(t, istr('Ai-1,R', i), mps{i}, istr('Ai,L', i));
        if ~isempty(oblabel{m, i})
            Astr = istr('Ai,P', i);
            Bstr = istr('H,U', i);
            idx = findhset(oblabel{m, i}, obset);
            t = contracttensors(t, Astr, obset{idx}, Bstr);
            t = renamemps(t, 'H,D', istr('Ai,P', i));
        end
        t = contracttensors(t, istr('Ai-1*,R,Ai,P', i), conjt(mps{i}), istr('Ai*,L,Ai*,P', i));
    end
    ob = ob + t.tensor;
end
        