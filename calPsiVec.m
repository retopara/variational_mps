function vec = calPsiVec(mps, N)
% ---------------------------------------------------------------
%  this function calculate state vector |Psi> as a d^N column array, has
%  not yet been tested, use with care.
% ---------------------------------------------------------------

V = mps{1};
for i = 1:N-1
    Astr = istr('Ai,R', i);
    Bstr = istr('Ai+1,L', i);
    A = contracttensors(V, Astr, mps{i+1}, Bstr);
    % merge Ai_P and Ai+1_P indices
    Alen = length(A.name);
    Arstr = split(istr('Ai+1,R', i));
    Alstr = split(istr('Ai,L', i));
    
    Aridx = find2(A, Arstr);
    Alidx = find2(A, Alstr);
    Apidx = 1:Alen;
    Apidx([Aridx, Alidx]) = [];
    
    Arsize = A.size(Aridx);
    Apsize = A.size(Apidx);
    Alsize = A.size(Alidx);
    
    Atmp = permute(A.tensor, [Aridx, Apidx, Alidx]);
    V.size = [prod(Arsize), prod(Apsize), prod(Alsize)];
    V.tensor = reshape(Atmp, V.size);
    V.name = split(istr('Ai+1,Ai+1,Ai+1', i));
    V.index = split('R,P,L');
end
vec = transpose(V.tensor);
