function t = geteye(d)

t.tensor = eye(d);
t.size = size(t.tensor);
t.name = split('H,H');
t.index = split('U,D');