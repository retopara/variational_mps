function [obset, oblabel, M] = getOBSet(N, type, J, h)
% ------------------------------------------------------
%  [obset, M] = getOBSet(N, Htype, h)
%
%  create the operators for calculate energy
%
%  Parameters
%   Htype       = Choose your hamiltonian
%   h           = The strength of magnet field
%
%  Returns
%   obset       = the set of observable operators
% -------------------------------------------------------
sx = [0, 1; 1, 0]; % 0.5 is the normalization coeff for sx
sy = [0, -1i;1i, 0];
sz = [1, 0; 0,-1];
s0 = [1, 0; 0, 1];

SX = 0.5 * sx;
SY = 0.5 * sy;
SZ = 0.5 * sz;

d = size(sx, 1);

obset = cell(20, 1);
obset{1}.label = 'Jsz';
obset{1}.tensor = J*sz;
obset{2}.label = 'Jsx';
obset{2}.tensor = J*sx;
obset{3}.label = 'Jsy';
obset{3}.tensor = J*sy;

obset{4}.label = 'sz';
obset{4}.tensor = sz;
obset{5}.label = 'sx';
obset{5}.tensor = sx;
obset{6}.label = 'sy';
obset{6}.tensor = sy;

obset{7}.label = 'hsx';
obset{7}.tensor = h*sx;
obset{8}.label = 'hsy';
obset{8}.tensor = h*sy;
obset{9}.label = 'hsz';
obset{9}.tensor = h*sz;
obset{10}.label = 'I';
obset{10}.tensor = eye(d);

for i = 1:length(obset)
    if ~isempty(obset{i})
        obset{i}.size = size(obset{i}.tensor);
        for j = 1:d
            obset{i}.name{j} = 'H';
            if j == 1
                obset{i}.index{j} = 'U';
            end
            if j == 2
                obset{i}.index{j} = 'D';
            end
        end
    else
        break;
    end
end

switch type
    case 'ising'
        % N-1+N terms
        %	Jsz	sz	I	I	I	I
        %	I	Jsz	sz	I	I	I
        %	I	I	Jsz	sz	I	I
        %				...
        %	I	I	I	I	Jsz	sz
        %	hsx	I	I	I	I	I
        %	I	hsx	I	I	I	I
        %	I	I	hsx	I	I	I
        %				...
        %	I	I	I	I	I	hsx


        M = N-1;
        if h ~= 0;
            M = M + N;
        end
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N-1
            oblabel{m, i} = 'Jsz';
            oblabel{m, i+1} = 'sz';
            m = m+1;
        end
        if h ~= 0
            for i = 1:N
                oblabel{m, i} = 'hsx';
                m = m+1;
            end
        end

    case 'xxz'
        % 2*(N-1)+N terms
        %	Jsx	sx	I	I	I	I
        %	I	Jsx	sx	I	I	I
        %	I	I	Jsx	sx	I	I
        %				...
        %	I	I	I	I	Jsx	sx
		
		%	Jsy	sy	I	I	I	I
        %	I	Jsy	sy	I	I	I
        %	I	I	Jsy	sy	I	I
        %				...
        %	I	I	I	I	Jsy	sy

        %	hsz	I	I	I	I	I
        %	I	hsz	I	I	I	I
        %	I	I	hsz	I	I	I
        %				...
        %	I	I	I	I	I	hsz

        M = 2*(N-1);
        if h ~= 0;
            M = M + N;
        end
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N-1
            oblabel{m, i} = 'Jsx';
            oblabel{m, i+1} = 'sx';
            m = m+1;
        end
        for i = 1:N-1
            oblabel{m, i} = 'Jsy';
            oblabel{m, i+1} = 'sy';
            m = m+1;
        end
        if h ~= 0
            for i = 1:N
                oblabel{m, i} = 'hsz';
                m = m+1;
            end
        end
        
    case 'ones'
        % 1 terms
        %	I	I	I	I	I	I

        M = 1;
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N
            oblabel{m, i} = 'I';
        end

    case 'sx'
        % N terms
        %	sx	I	I	I	I	I
        %	I	sx	I	I	I	I
        %	I	I	sx	I	I	I
        %				...
        %	I	I	I	I	I	sx

        M = N;
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N
            oblabel{m, i} = 'sx';
            m = m+1;
        end

    case 'sz'
        % N terms
        %	sz	I	I	I	I	I
        %	I	sz	I	I	I	I
        %	I	I	sz	I	I	I
        %				...
        %	I	I	I	I	I	sz

        M = N;
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N
            oblabel{m, i} = 'sz';
            m = m+1;
        end

    case 'sxx'
        % N-1 terms
        %	sx	sx	I	I	I	I
        %	I	sx	sx	I	I	I
        %	I	I	sx	sx	I	I
        %				...
        %	I	I	I	I	sx	sx
        
        M = N-1;
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N-1
            oblabel{m, i} = 'sx';
            oblabel{m, i+1} = 'sx';
            m = m+1;
        end

    case 'szz'
        % N-1 terms
        %	sz	sz	I	I	I	I
        %	I	sz	sz	I	I	I
        %	I	I	sz	sz	I	I
        %				...
        %	I	I	I	I	sz	sz
        
        M = N-1;
        oblabel = cell(M, N);
        m = 1;
        for i = 1:N-1
            oblabel{m, i} = 'sz';
            oblabel{m, i+1} = 'sz';
            m = m+1;
        end

    otherwise
        error('getOBSet:WrongOBType', '\n [getOBSet] ERROR: OBtype %s is wrong!', type);
end